FROM ubuntu:20.04
# Use CRAN to get latest R version
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get -qq update && apt-get install -y --no-install-recommends \
    gnupg2 \
    ca-certificates
RUN echo "deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

# Install repository tools - python3, pip3, curl, JRE, wget, R and VCFTools
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get -qq update && apt-get install -y --no-install-recommends \
    bc \
    curl \
    g++ \
    gcc \
    gfortran \
    libblas-dev \
    liblapack-dev \
    make \
    file \
    openjdk-8-jre-headless \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    python3-gdal \
    r-base \
    vcftools \
    libcurl4-openssl-dev \
    libssl-dev \
    libxml2-dev \
    git \
    flex \
    bison \
    libgmp3-dev \
    cmake \
    raxml \
    mrbayes \
    ncbi-blast+ \
    wget && \
    ln -fs /usr/share/zoneinfo/Europe/Lisbon /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get clean

## Install software not available in Ubuntu repositories
WORKDIR /RRL/software
# Install *Structure_threader*
RUN pip3 install structure_threader==1.3.9
# Install Toytree
RUN pip3 install toytree==2.0.1

# Install Baypass
RUN curl http://www1.montpellier.inra.fr/CBGP/software/baypass/files/baypass_2.2.tar.gz -L -o baypass.tar.gz;\
    tar xvfz baypass.tar.gz;\
    cd baypass_2.2/sources;\
    make FC=gfortran;\
    mv ./g_baypass ../../;\
    rm -rf baypass_2.2 baypass.tar.gz

# Install PGDSpider
RUN curl http://www.cmpg.unibe.ch/software/PGDSpider/PGDSpider_2.1.1.5.zip -L -o pgdspider.zip;\
    unzip pgdspider.zip;\
    mv PGDSpider_2.1.1.5/PGDSpider2-cli.jar ./;\
    rm -rf PGDSpider_2.1.1.5 pgdspider.zip

# Install RAxML-NG
RUN git clone --recursive https://github.com/amkozlov/raxml-ng;\
    cd raxml-ng;\
    git checkout 1.0.2;\
    mkdir build && cd build;\
    cmake ..;\
    make;\
    mv ../bin/raxml-ng /usr/local/bin/;\
    cd ../.. && rm -rf raxml-ng

# Install R-packages
RUN R -e "install.packages('Hmisc', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('caret', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('gplots', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('IDPmisc', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('fields', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('maps', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('RColorBrewer', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('corrplot')"\
    && R -e "install.packages('ape')"\
    && R -e "install.packages('https://cran.r-project.org/src/contrib/Archive/mvtnorm/mvtnorm_1.0-8.tar.gz', repos=NULL, type='source')"\
    && R -e "install.packages('geigen')"\
    && R -e "install.packages('BiocManager')"\
    && R -e "BiocManager::install('SNPRelate')"

# Install ALStructure
RUN R -e "install.packages('devtools', repos='https://cloud.r-project.org/')"\
    && R -e "devtools::install_github('storeylab/alstructure', build_vignettes=FALSE, ref='e355411')"\
    $$ R -e "BiocManager::install('lfa')"

# Install DIAMOND 2.0.15
RUN wget https://github.com/bbuchfink/diamond/releases/download/v2.0.15/diamond-linux64.tar.gz;\
    tar xzf diamond-linux64.tar.gz;\
    mv diamond /usr/local/bin


## The good stuff!
# Get the analyses framework
WORKDIR /RRL
COPY . ./

