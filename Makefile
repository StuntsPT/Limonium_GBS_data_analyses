# Makefile for $(run_name) GBS dataset analyses
# First off, define some global variables:

# Run name - Set the name for this run
run_name=Limonium_species
# Default subset
loci_subset=All_loci
# Original vcf filename
original_vcf=SNPs.vcf
# Number of threads to use on all the threaded analyses
threads=8
# How much missing data can each of our individuals have (in percentage)?
miss_thres_percent=60
missing_threshold=$(shell echo $(miss_thres_percent) / 101 | bc -l)
# Min "Minor Allele Frequency" allowed:
MAF=0.01
SHORT_MAF=$(shell echo $(MAF) |sed 's|0\.||')
# Max missing data per SNP, **after** individual filtering
MAX_MISSING=0.80
SHORT_MAX_MISSING=$(shell echo $(MAX_MISSING) |sed 's|0\.||')
# Default VCF file to use
VCF_in_use=$(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
# Number of Ks to calculate
K=8
# Number of STRUCTURE replicates per "K"
#Reps=20
# Bayesfactor threshold for RONA analysis
ASSOC_BF=20
# Random seed for whatever requests it
SEED=112358
# Get the current working directory
wd=$(dir $(abspath Makefile))
# Bioclim data location
BIOCLIM=$(wd)mapdata
# Outputs location
OUTDIR=$(wd)outfiles/$(run_name)/
# Sample coordinates file
SAMPLE_COORDS=$(wd)infiles/$(run_name)/Sample_coords.tsv
# PCA explained variance threshold (for transforming env vars in PCA eigenvectors)
PCA_THRESHOLD=0.85
# Population map (individual to population matching)
POPMAP=infiles/$(run_name)/Individual_pops_map.txt
# RAxML binary
RAxML_BINARY=raxmlHPC-PTHREADS-AVX
# Phylip file max missing data percent
PHYLIP_MAX_MISSING=58
# MrBayes number of iterations
MBNGEN=1000000
# Target transcriptome accession number
TRANSCRIPTOME_ACCESSION=SRR12432533

# Now, set the rules:
all : filtering clustering neutral_clustering selection_clustering

BIOCLIMS := $(shell seq 1 1 19| sed 's/^[[:digit:]]$$/0&/')

CHELSA := $(patsubst %,$(BIOCLIM)/CHELSA_bio10_%.tif,$(BIOCLIMS))


$(BIOCLIM)/CHELSA_bio10_%.tif :
	mkdir -p $(BIOCLIM)
	wget https://envicloud.os.zhdk.cloud.switch.ch/chelsa/chelsa_V1/climatologies/bio/$(shell echo "$@" | sed 's|.*/||' ) -O $@


$(OUTDIR)Climdata_by_location.tsv : $(SAMPLE_COORDS) $(CHELSA)
	# Create new directory for results
	mkdir -p $(OUTDIR)
	# Extract bioclimatic data from the samples' coordinates
	cut -f 1 $< > $(BIOCLIM)/temp_holder.tsv
	bin/extract_layer_data.sh $< $@ $(BIOCLIM)
	sed -i '1 s|$(BIOCLIM)/CHELSA_||g; 1 s|\.tif||g; s|bio10_|bio_|g' $@


$(OUTDIR)Climdata_by_location_uncorr.tsv : $(OUTDIR)Climdata_by_location.tsv $(SAMPLE_COORDS)
	# Remove correlated variables from present conditions file
	Rscript bin/variable_reduction.R $< $(PCA_THRESHOLD) $@


$(OUTDIR)Climdata_by_location_uncorr_per_pop.tsv : $(OUTDIR)Climdata_by_location_uncorr.tsv $(POPMAP)
	# Merge individual data into populaton data
	python3 bin/indiv_to_merged_pops.py $(word 2,$^) $(word 1,$^) > $@


$(OUTDIR)ENVFILE : $(OUTDIR)Climdata_by_location_uncorr_per_pop.tsv
	# Remove the column names, and transpose to have a Baypass compatible file
	cut -f 2- $< | awk -f bin/transpose.awk > $@


$(OUTDIR)Filtered_VCFs/$(run_name).imiss : infiles/$(run_name)/$(original_vcf)
	# Create new directory for results
	mkdir -p $(OUTDIR)Filtered_VCFs
	
	# Check for missing data
	bin/vcftools --vcf infiles/$(run_name)/$(original_vcf) --missing-indv --out $(OUTDIR)Filtered_VCFs/$(run_name)


$(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).vcf : $(OUTDIR)Filtered_VCFs/$(run_name).imiss
	# Check which individuals have too much missing data
	$(eval indivs_to_remove=$(shell bin/select_low_rep_individuals.sh $(OUTDIR)Filtered_VCFs/$(run_name).imiss $(missing_threshold) | sed 's|^| --remove-indv |g' |tr -d '\n' ))
	
	# Remove individuals with too much missing data
	bin/vcftools --vcf infiles/$(run_name)/$(original_vcf) $(indivs_to_remove) --out $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent) --recode
	mv $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).recode.vcf $@


$(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.vcf : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).vcf
	# Remove SNPs with too much missing data, very low MAF and more than biallelic
	bin/vcftools --vcf $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).vcf  --max-missing $(MAX_MISSING) --maf $(MAF) --out $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele --recode --max-alleles 2
	mv $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.recode.vcf $@


$(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.vcf
	# Keep only the Center SNP and discard all others to avoid LD issues
	# Our final VCF file is now produced.
	python3 bin/vcf_parser.py --center-snp -vcf $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.vcf
	mv $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_bialleleCenterSNP.vcf $@


$(OUTDIR)Popfiles/$(run_name)_individuals.txt : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
	# Create a file with the names of the individuals that were retained in the filtered VCF
	mkdir -p $(OUTDIR)Popfiles
	bin/get_indivs_from_vcf.sh $< $@


$(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt : $(OUTDIR)Popfiles/$(run_name)_individuals.txt $(POPMAP)
	# Create a popinfo file based on the retained individuals only
	$(shell grep -f $< $(word 2,$^) |sort > $@)


$(OUTDIR)Popfiles/$(run_name)_popnames_single.txt : $(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt
	# Create a file that contains only the names of the present populations
	$(shell cut -f 2 $< |uniq > $@)


$(OUTDIR)Popfiles/$(run_name)_structure_sorting_popfile.txt : infiles/$(run_name)/population_order.tsv $(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt
	# Create a file to allow Structure_threader to know how many individuals are in each population and in which order they should be presented
	set -e;\
	while read f ; do \
	_pop=$$(echo $$f | cut -f 1 -d " " | cut -f 1);\
	_COUNT=$$(grep -c $$_pop $(word 2,$^));\
	_POS=$$(echo $$f | cut -f 2 -d " " | cut -f 2);\
	echo "$$_pop\t$$_COUNT\t$$_POS" | grep -v "\t0\t" >> $@;\
	done < $<


$(OUTDIR)PCA/$(loci_subset)/$(run_name)_PCA.pdf : $(VCF_in_use) $(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt
	# Create a directory to store outputs
	mkdir -p $(OUTDIR)PCA/$(loci_subset)
	
	# Perform a PCA analysis from the VCF file
	Rscript bin/snp_pca_static.R $(VCF_in_use) $@ $(word 2,$^) |grep -A 4 'snpgdsPCAClass' |tail -n +2 > $(OUTDIR)PCA/$(loci_subset)/$(run_name)_eigenvectors.txt
	mv $@.pdf $@


$(OUTDIR)Bayescan/$(run_name).geste : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf $(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt Static_param_files/skeleton.spid
	# Create a directory to store outputs
	mkdir -p $(OUTDIR)Bayescan
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(word 2,$^)|g' $(word 3,$^) > $(OUTDIR)Popfiles/vcf2geste.spid
	echo "" >> $(OUTDIR)Popfiles/vcf2geste.spid
	echo "WRITER_FORMAT=GESTE_BAYE_SCAN" >> $(OUTDIR)Popfiles/vcf2geste.spid
	echo "" >> $(OUTDIR)Popfiles/vcf2geste.spid
	echo "GESTE_BAYE_SCAN_WRITER_DATA_TYPE_QUESTION=SNP" >> $(OUTDIR)Popfiles/vcf2geste.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf -inputformat VCF -outputfile $@ -outputformat GESTE -spid $(OUTDIR)Popfiles/vcf2geste.spid


$(OUTDIR)Structure/$(loci_subset)/$(run_name).structure : $(VCF_in_use) $(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt Static_param_files/skeleton.spid
	# Create a directory to store outputs
	mkdir -p $(OUTDIR)Structure/$(loci_subset)
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(word 2,$^)|g' $(word 3,$^) > $(OUTDIR)Popfiles/vcf2structure.spid
	echo "" >> $(OUTDIR)Popfiles/vcf2structure.spid
	echo "WRITER_FORMAT=STRUCTURE" >> $(OUTDIR)Popfiles/vcf2structure.spid
	echo "" >> $(OUTDIR)Popfiles/vcf2structure.spid
	echo "STRUCTURE_WRITER_LOCI_DISTANCE_QUESTION=false" >> $(OUTDIR)Popfiles/vcf2structure.spid
	echo "STRUCTURE_WRITER_DATA_TYPE_QUESTION=SNP" >> $(OUTDIR)Popfiles/vcf2structure.spid
	echo "" >> $(OUTDIR)Popfiles/vcf2structure.spid
	echo "STRUCTURE_WRITER_FAST_FORMAT_QUESTION=true" >> $(OUTDIR)Popfiles/vcf2structure.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile $(wd)$(VCF_in_use) -inputformat VCF -outputfile $@ -outputformat STRUCTURE -spid $(OUTDIR)Popfiles/vcf2structure.spid


## Run ALStructure
$(OUTDIR)Structure/$(loci_subset)/ALStructure_results/: $(VCF_in_use) $(OUTDIR)Popfiles/$(run_name)_structure_sorting_popfile.txt
	# Run ALStructure wrapped under Structure_threader
	bin/structure_threader run -i $< -o $(OUTDIR)Structure/$(loci_subset)/ALStructure_results -als bin/alstructure_wrapper.R -K $(K) -t $(threads) --pop $(word 2,$^)


$(OUTDIR)Baypass/$(run_name).baypass : $(OUTDIR)Bayescan/$(run_name).geste
	# Convert geste to baypass format
	mkdir -p $(OUTDIR)Baypass
	python3 bin/geste2baypass.py $< $@


$(OUTDIR)Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out : $(OUTDIR)Baypass/$(run_name).baypass $(OUTDIR)ENVFILE $(OUTDIR)Popfiles/$(run_name)_popnames_single.txt $(OUTDIR)Bayescan/$(run_name).geste
	# Prepare the wrapper program by defining the required variables
	sed 's|source("")|source("$(wd)bin/baypass_utils.R")|' bin/Baypass_workflow.R > $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|baypass_executable = ""|baypass_executable = "$(wd)bin/g_baypass"|' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|popname_file = ""|popname_file = "$(word 3,$^)"|' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|envfile = ""|envfile = "$(word 2,$^)"|' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|geno_file = ""|geno_file = "$(word 1,$^)"|' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|prefix = ""|prefix = "$(run_name)"|' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's?num_pops =?num_pops = $(shell head -n 3 $(word 4,$^) | tail -n 1 |sed "s/.*=//")?' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's?num_SNPs =?num_SNPs = $(shell head -n 1 $(word 4,$^) | sed "s/.*=//")?' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|num_threads =|num_threads = $(threads)|' $(OUTDIR)Baypass/Baypass_workflow.R
	sed -i 's|scale_cov = TRUE|scale_cov = TRUE|' $(OUTDIR)Baypass/Baypass_workflow.R
	
	# Run Baypass
	cd $(OUTDIR)Baypass && Rscript Baypass_workflow.R

$(OUTDIR)Baypass/$(run_name)_associations_summary.txt : $(OUTDIR)Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out
	# Gather a summary of associations
	python3 bin/associations_gatherer.py $< $(ASSOC_BF) > $@


$(OUTDIR)Filtered_VCFs/$(run_name)_neutrals.vcf : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf $(OUTDIR)Baypass/$(run_name)_associations_summary.txt
	python3 bin/outlier_removal.py neutral -a $(OUTDIR)Baypass/$(run_name)_associations_summary.txt -v $< > $@


$(OUTDIR)Filtered_VCFs/$(run_name)_selection.vcf : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf $(OUTDIR)Baypass/$(run_name)_associations_summary.txt
	python3 bin/outlier_removal.py selection -a $(OUTDIR)Baypass/$(run_name)_associations_summary.txt -v $< > $@


# Phylogenetics
$(OUTDIR)RAxML/$(run_name)_filtered.phy : infiles/$(run_name)/$(run_name).phy $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
	# Remove sequences with too much missing data
	mkdir -p $(OUTDIR)RAxML
	#python3 bin/phylip_missing_remover.py $< $(PHYLIP_MAX_MISSING) > $@
	grep "^#CHR" $(word 2,$^) |cut -f 10- |tr "\t" "\n" > indlist.txt
	head -n 1 $(word 1,$^) |cut -d " " -f 2 > seqlen
	wc -l < indlist.txt | paste -d " " - seqlen > $@
	grep -f indlist.txt $(word 1,$^) >> $@


$(OUTDIR)RAxML/RAxML_bipartitions.$(run_name) : $(OUTDIR)RAxML/$(run_name)_filtered.phy
	# Run RAxML
	mkdir -p $(OUTDIR)RAxML
	cd $(OUTDIR)RAxML && $(RAxML_BINARY) -f a -m GTRCAT -p $(SEED) -x $(SEED) -N autoMRE -s $< -n $(run_name) -T $(threads)


$(OUTDIR)RAxML/RAxML_full_names.tre : infiles/$(run_name)/Phylogenetics_map.txt $(OUTDIR)RAxML/RAxML_bipartitions.$(run_name)
	# Obtian a tree file with plain English names instead of codenames
	python3 bin/phylo_mapper.py $(word 1,$^) $(word 2,$^) > $@


$(OUTDIR)RAxML/RAxML_$(run_name)_tree.svg : $(OUTDIR)RAxML/RAxML_full_names.tre infiles/$(run_name)/phylo_groups.json
	# Plot the tree
	python3 bin/treeplotter.py $(word 1,$^) $(word 2,$^) $@


$(OUTDIR)Lbicolor_t/Lbicolor_t.fna :
	# Obtain the L. bicolor transcriptome and uncompress it
	mkdir -p $(OUTDIR)Lbicolor_t
	wget https://sra-download.ncbi.nlm.nih.gov/traces/wgs03/wgs_aux/GB/RK/GBRK01/GBRK01.1.fsa_nt.gz -O $(OUTDIR)Lbicolor_t/GBRK01.1.fsa_nt.gz
	cd $(OUTDIR)Lbicolor_t && gunzip GBRK01.1.fsa_nt.gz && mv GBRK01.1.fsa_nt $@


$(OUTDIR)Lbicolor_t/Lbicolor_t.nin : $(OUTDIR)Lbicolor_t/Lbicolor_t.fna
	# Create BLAST datbases from the FASTA file
	cd $(OUTDIR)Lbicolor_t && makeblastdb -in $(word 1,$^) -dbtype nucl -title Lbicolor_t -out Lbicolor_t -parse_seqids


$(OUTDIR)Lbicolor_t/segregating_loci_transcriptome_blast.tab : $(OUTDIR)Lbicolor_t/Lbicolor_t.nin $(OUTDIR)Annotations/segregated_loci.fasta
	# Find matches between the segregated loci and the transcriptome
	cd $(OUTDIR)Lbicolor_t && blastn -db Lbicolor_t -query $(word 2,$^) -out $@ -num_threads $(threads) -outfmt 7


$(OUTDIR)Lbicolor_t/transcriptome_matched_loci.fasta : $(OUTDIR)Lbicolor_t/segregating_loci_transcriptome_blast.tab
	# Obtain a FASTA with the matched transcriptome sequences
	grep "^vcf" $(word 1,$^) |cut -f 2 > $(OUTDIR)Lbicolor_t/matched_ids.txt
	cd $(OUTDIR)Lbicolor_t && blastdbcmd -db Lbicolor_t -dbtype nucl -entry_batch matched_ids.txt > $@


$(OUTDIR)Lbicolor_t/loci_to_annotate.fasta : $(OUTDIR)Lbicolor_t/segregating_loci_transcriptome_blast.tab $(OUTDIR)Annotations/segregated_loci.fasta $(OUTDIR)Lbicolor_t/transcriptome_matched_loci.fasta
	# Merge the loci FASTA with the transcriptome sequences
	grep "^vcf" $(word 1,$^) | cut -f 1 | sed 's/^/>/' > $(OUTDIR)Lbicolor_t/segregated_loci_to_remove.txt
	# Thanks to https://stackoverflow.com/a/55637258/3091595 !
	awk '(NR==FNR) { toRemove[$$1]; next } /^>/ { p=1; for(h in toRemove) if ( h ~ $$0) p=0 } p' $(OUTDIR)Lbicolor_t/segregated_loci_to_remove.txt $(word 2,$^) | cat - $(word 3,$^) > $@
	grep "^vcf" $(word 1,$^) | cut -f 1,2 | while read lines; do _v1=$$(echo $$lines | cut -d " " -f 1); _v2=$$(echo $$lines | cut -d " " -f 2); sed -i "s/$${_v2}/&$${_v1}/" $@; done


$(OUTDIR)Databases/uniparc_active.fasta.gz : 
	# Obtain uniparc DB
	mkdir -o $(OUTDIR)Databases
	wget https://ftp.expasy.org/databases/uniprot/current_release/uniparc/uniparc_active.fasta.gz -O $@


$(OUTDIR)Databases/uniparc_db.dmnd : $(OUTDIR)Databases/uniparc_active.fasta.gz
	# Convert fasta to diamond db
	cd $(OUTDIR)Databases && zcat $(word 1,$^) | diamond makedb -d uniparc_db


$(OUTDIR)Annotations/annotations.tab : $(OUTDIR)Databases/uniparc_db.dmnd $(OUTDIR)Lbicolor_t/loci_to_annotate.fasta
	# Match final loci list to uniparc for further annotation
	mkdir -p $(OUTDIR)Annotations
	cd $(OUTDIR)Databases && diamond blastx -d uniparc_db -q $(word 2,$^) -o $@ --sensitive


$(OUTDIR)Annotations/segregated_loci.txt : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
	# Obtain a list of loci 100% segregated between L. vulgare and L. maritimum
	mkdir -p $(OUTDIR)Annotations
	python3 bin/segregating_loci_finder.py $(word 1,$^) 7 | tail -n +2 > $@


$(OUTDIR)Annotations/segregated_loci.fasta : $(wd)infiles/$(run_name)/loci.loci $(OUTDIR)Annotations/segregated_loci.txt
	# Obtain a FASTA file from the loci consensus
	mkdir -p $(OUTDIR)Annotations
	python3 bin/loci_consensus.py -in $(word 1,$^) -o $@ -l $(word 2,$^)
	

# "Meta" rules
.PHONY : all clean filtering clustering neutral_clustering PCA neutral_pca structure neutral_structure phylogenetics

clean :
	# Clear EVERYTHING!
	rm -rf $(OUTDIR)*

filtering : $(OUTDIR)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf $(OUTDIR)Popfiles/$(run_name)_popinfo_names.txt

clustering : PCA structure

neutral_clustering : neutral_pca neutral_structure

selection_clustering : selection_pca selection_structure
	
PCA : $(OUTDIR)PCA/$(loci_subset)/$(run_name)_PCA.pdf 

structure : $(OUTDIR)Structure/$(loci_subset)/ALStructure_results/

phylogenetics : $(OUTDIR)RAxML/RAxML_$(run_name)_tree.svg

neutral_pca : $(OUTDIR)Filtered_VCFs/$(run_name)_neutrals.vcf
	$(MAKE) PCA VCF_in_use=$< loci_subset=Neutrals

neutral_structure : $(OUTDIR)Filtered_VCFs/$(run_name)_neutrals.vcf
	$(MAKE) structure VCF_in_use=$< loci_subset=Neutrals

selection_pca : $(OUTDIR)Filtered_VCFs/$(run_name)_selection.vcf
	$(MAKE) PCA VCF_in_use=$< loci_subset=Selection

selection_structure : $(OUTDIR)Filtered_VCFs/$(run_name)_selection.vcf
	$(MAKE) structure VCF_in_use=$< loci_subset=Selection
