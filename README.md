# Limonium data analyses


## In this repository you will find:

* A *Makefile* describing the data analysis process
* A *Dockerfile* describing how to build a docker container to analyse the data
* A *bin* directory, containing some custom scripts as well as symlinks for the used software (tailored for the Docker image)
* A *Static_param_files* directory containing static configuration files that work as the analysis backbone
* A *submodules* directory containing clones of other git repositories with scripts used in the analyses


## What you won't find here:

* Input data files
* Output files (AKA, analyses results)

## Running the analyses:

1. Get the docker image:

``` bash
docker pull stunts/limonium_data_analyses
```

2. Start a shell in the docker container. The volumes containing the input files and output directory should be mounted in this stage:

``` bash
docker run -v /data/Limonium/analyses_infiles:/RRL/infiles -v /data/Limonium/analyses_outfiles:/RRL/outfiles -i -t stunts/limonium_data_analyses:general_002
```

Of course, this should be adapted to your own file system file locations!

3. Once inside the interactive docker shell, you can run the analyses themselves. In order to match what was used in the paper:

``` bash
# Species dataset
make run_name=Limonium_species /RRL/outfiles/RAxML/RAxML_Limonium_species_tree.svg clustering

# Lvu_Lma dataset
make run_name=Limonium_lvu_lma miss_thres_percent=90 /RRL/outfiles/Limonium_lvu_lma/Annotations/annotations.tab clustering
```

This might take a while, but in the end you will have all the analyses results, 100% reproducible!

## Warning:
Some steps are likely to take a considerable amount of time to run, namely the RAxML analysis, and the Annotation step.
The annotation step will download the Uniparc database for the annotations. At the time of writing, this database was 100GB (Uniparc release 2022_01). It will certainly increase with time. Also, I am not aware of a way to obtain specific Uniparc releases, which might affect future reproducibility.
