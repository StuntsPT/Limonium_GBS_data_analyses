#!/usr/bin/env python3

import numpy as np
from sys import argv

files = argv[1:-1]

array_list = []
for tsv in files:
    array_list.append(np.genfromtxt(tsv, skip_header=True, dtype=float)[:,1:])

avg = np.mean( np.array(array_list), axis=0 )

np.savetxt(argv[-1], avg, delimiter="\t")
