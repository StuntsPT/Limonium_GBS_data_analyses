#!/usr/bin/env python3

from sys import argv

with open(argv[2], "r") as treehandle:
    treedata = treehandle.readline()

mapfile = open(argv[1], "r")
for lines in mapfile:
    lines = lines.strip().split("\t")
    treedata = treedata.replace(lines[0], lines[1])
mapfile.close()

print(treedata)

# Usage: python phylo_mapper.py Phylogenetics_map.txt raxml_tree_file.whatever
