#!/usr/bin/env python3

def read_translation(translation_filename):
    """
    Reads a translation file and returns a dict with the data.
    """
    infile = open(translation_filename, 'r')
    translations = {}
    for lines in infile:
        lines = lines.split()
        translations[lines[0]] = " ".join(lines[1:])

    infile.close()

    return translations


def replacer(translations, file_to_replace):
    """
    Replaces individual names with their coordinates in a genepopfile according
    to a translation table.
    """
    infile = open(file_to_replace, 'r')
    for lines in infile:
        if "," in lines:
            name = lines[:lines.index(",")].strip()
            print(lines.replace(name, translations[name]), end="")
        else:
            print(lines, end="")


if __name__ == "__main__":
    from sys import argv
    # Usage: python3 translation_file genepop_file.txt
    TRANS = read_translation(argv[1])
    replacer(TRANS, argv[2])
