#!/usr/bin/env python3

# Removes sequences above a determined % of missing data from a phylip file.
# Reads from a file and writes to STDOUT
# Usage: python3 phylip_missing_remover.py path/to/file.phy missing_threshold

from sys import argv

miss_threshold = int(argv[2])
infile = open(argv[1], 'r')

seqlen = int(infile.readline().split()[1])
absolute_threshold = seqlen * miss_threshold / 100

printed_seqs = []

printed_seqs = [lines for lines in infile
                if lines.split()[1].count('N') < absolute_threshold]

print("{} {}".format(len(printed_seqs), seqlen))
for line in printed_seqs:
    print(line, end="")
