#!/usr/bin/env python3
"""
This module plots phylogenetic trees from a
RAxML `bipartitions` output file.
"""

import itertools
import json
from sys import argv
import toytree
import toyplot
import toyplot.svg

# Set a palette and bootstrap color intervals
# from https://colorbrewer2.org/#type=diverging&scheme=Spectral&n=7
#COL_PALETTE = ['rgb(213,62,79)','rgb(252,141,89)','rgb(254,224,139)',
#              'rgb(255,255,191)','rgb(153,213,148)','rgb(26,152,80)',
#              'rgb(50,136,189)']
#
#BOOT_INTERVALS = (30, 20, 20, 10, 10, 10, 1)

# https://colorbrewer2.org/#type=diverging&scheme=PRGn&n=7 (`rm` center color)
COL_PALETTE = ['rgb(118,42,131)','rgb(175,141,195)','rgb(247,247,247)',
               'rgb(217,240,211)','rgb(127,191,123)', 'rgb(27,120,55)']
BOOT_INTERVALS = (50, 20, 10, 10, 10, 1)


def info_gatherer(tree_file, groups_file):
    """
    Gathers information from the input files:
    `tree_file` is the path to a RAxML `bipartitions` file and
    `groups_file` is the path to a JSON file containing group information for
    both taxa colours and rooting.
    Retuns a toytree.tree() object containing the basic tree as read from
    the input file and a dict containing group information based on taxa
    prefixes.
    """
    my_tree = toytree.tree(tree_file)
#    grp_colrs = {"LCR": "#ffd700", "LCL": "#ffd700", "LBA": "#ffd700",
#                 "LHU": "#008b8b",
#                 "LMA": "#ff00ff",
#                 "LNA": "#228b22",
#                 "LVU": "#2a52be"}
    try:
        with open(groups_file) as json_data:
            groups_data = json.load(json_data)
    except json.decoder.JSONDecodeError:
        print("Invalid JSON detected. Ignoring user defined groups.")
        grp_colrs = None
    else:
        try:
            grp_colrs = groups_data["group_colors"]
        except KeyError:
            print("No group information found in JSON - all taxa will be drawn"
                  " in black.")
            grp_colrs = None
            my_tree = my_tree.ladderize()

#    my_tree = my_tree.root(regex="(?!(LVU|LMA|LNA)).*").ladderize()
        try:
            my_tree = my_tree.root(regex=f"({'|'.join(groups_data['root_groups'])}).*").ladderize()
        except KeyError:
            print("No rooting information found in JSON - rooting will be left"
                  " at first taxon.")
            my_tree = my_tree.ladderize()

    return my_tree, grp_colrs


def tree_plotter(tre, group_colours, save_location):
    """
    Plots the trees from the input objects
    `tre` is a toytree.tree() object
    `group_colours` is a dict where the keys are taxonomic group prefixes
    and the values are the respective colours
    The third argument, `save_location` is the path to where the resulting
    SVG file should be written.
    """
    node_palette = [COL_PALETTE[idx:idx+1] * interval
                    for idx, interval in zip(range(len(COL_PALETTE)), BOOT_INTERVALS)]
    node_palette = list(itertools.chain.from_iterable(node_palette))

    colors = [node_palette[i] for i in tre.get_node_values('support', 1, 1)]

    my_node_sizes = [12 if i else 0 for i in tre.get_node_values()]

    try:
        tip_colorlist = []
        for tip in tre.get_tip_labels():
            for k, v in group_colours.items():
                if k in tip:
                    tip_colorlist.append(v)
                    break
            else:
                tip_colorlist.append("#000000")
    except AttributeError:
        tip_colorlist = "#000000"

    canvas, axes, mark = tre.draw(width=1300, tip_labels_align=False,
                                  node_colors=colors, scalebar=True,
                                  tree_style="n", node_sizes=my_node_sizes,
                                  tip_labels_colors=tip_colorlist,
                                  edge_widths=3,
                                  height=len(tre.get_tip_labels()) * 15)

    bootstraps = list(map(lambda x: f"Bootstrap ≤ {x - 1}",
                      itertools.accumulate(BOOT_INTERVALS)))

    bootstraps[-1] = bootstraps[-1].replace("≤", "=")

    markers = [toyplot.marker.create(shape="o", size=18, mstyle={"fill": x}) for x in COL_PALETTE]

    legend_markers = list(zip(bootstraps, markers))

    canvas.legend(legend_markers,
                  corner=("top-right", 20, 170, 140))

    toyplot.svg.render(canvas, save_location)


if __name__ == "__main__":
    # Usage: python treeplotter.py /path/to/bipartitions_file
    # /path/to/file.json /path/to/output.svg
    tree, groups = info_gatherer(argv[1], argv[2])
    tree_plotter(tree, groups, argv[3])
