#!/bin/bash

wget "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=BA000009.3&rettype=fasta" -O Beta_vulgaris_mtDNA.fasta

wget "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&rettype=fasta&id=MN599096.1" -O L.sinense_cpDNA.fasta

cat Beta_vulgaris_mtDNA.fasta L.sinense_cpDNA.fasta > bait_reference.fasta
